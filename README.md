# R objects

R scripts in the ``R`` directory each create an R-object stored in the filehash database in the ``rdata`` directory.

Create dependencies for the R objects,
```Shell
$ Rscript dependencies.R dependencies.inc
```

Run the makefile to create the R objects,
```Shell
$ make
```

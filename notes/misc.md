# Shrinkage

Suppose $\beta_t \sim N(0, \sigma^2)$ an $\beta_t \sim N(\beta_{t-1}, \tau_2)$, then
$$
\begin{aligned}[t]
p(\beta_t | .) = N(0, \sigma_1^2) N(\beta_{t-1}, \tau^2) \\
\propto \exp \left(-\frac{1}{2 \sigma^2} \beta_t^2 \right) \left(-\frac{1}{2 \tau^2} (\beta_t - \beta_{t-1})^2 \right) \\
= \exp \left( -\frac{\beta_t^2}{2 \sigma^2} - \frac{1}{2 \tau^2} (\beta_t^2 + \beta_{t-1}^2 - 2 \beta_t \beta_{t-1}) \right)
\end{aligned}
$$
where
$$
\begin{aligned}[t]
\beta_t^* = \frac{\beta_{t-1}}{\tau^2} \omega^{-2} \\
\omega^2 = \left(\frac{1}{\sigma^2} + \frac{1}{\tau^2} \right)^{-1}
\end{aligned}
$$
or
$$
\beta_t^* = \frac{\sigma^}{\sigma^2 + \tau^2} \beta_{t-1}
$$


# Models


- Random effects

     - dyad
     - country
     - war

- Model parameters as AR(1) (generally around non-zero mean)
- Try a Polynomial basis
- Try a Gaussian Process model --- perhaps with the a non-zero mean function.
- Discrete dependent variables. It is most natural to

    - How to model measurement error?
    - This would cause problems with missing data and make using Stan infeasiable. If casualties are strengths are treated as parameters in order to impute missing data they



R = Rscript
RDATA = rdata
R_FILES = $(wildcard R/*.R)


all: ROBJECTS

ROBJECTS: $(robjects)
	@echo $(robjects)

dependencies.inc: dependencies.R $(R_FILES)
	$(R) $< $@

clean-rdata:
	-rm -f $(RDATA)/*

rdata/%: R/%.R
	$(R) run.R $<


-include dependencies.inc

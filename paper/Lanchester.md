---
title: Empirical Models of Battles
author: Jeffrey B. Arnold
version: 0.0.1
draftstatus: draft
bibliography: <!-- \bibliography{jabref.bib} /-->
keywords: ["Lanchester model", "battles", "military technology"]
rights: "2014 Jeffrey B. Arnold, CC BY"
affiliations: Instructor of Political Science, Emory University; Ph.D. Candidate in Political Science, University of Rochester.
thanksnote: |
  Earlier versions of this were presented at the Annual Meeting of the Midwest Political Science Association 2012, 2013.
abstract: |
  Abstract to go here.

---





# Introduction {#intro}

How has the technology of war changed over time?  And prior to that
question, how to define the technology of war?  I define the
technology of war as the process that generates attrition. More
specifically, it is the production function that maps the forces of
the belligerents to casualties. This definition is consistent with the
Lanchester model of attrition.  I estimate a Bayesian hierarchical
model of casualties for a set of battles between 1600 and 1982.  The
hierarchical model provides estimates of the parameters of the
Lanchester model for each war and battle, allowing for a comparison of
the technology of attrition across wars.

Attrition can be considered a technology analogous to economic
production.
^[A point made in @Hirshleifer1991 and @Hirshleifer2000]
The technology of war is the attrition process, represented as a
production function between the inputs --- the forces of the
belligerents --- and the outputs --- the casualties of those forces.
The technology of war at any point in time are the parameters of that
function.  And technological change is a change in the parameters of
the function.  At its most abstract, combat is a function that maps
military forces into casualties, and the technology of combat is the
shape of that function.  Other aspects of war, from the strategic
decisions to enter or exit, to the operational decisions of when or
where to fight with how many, and when to disengage from a battle are
strategic decisions made by the belligerents. And those strategic
decisions condition on the technology of war, because that determines
what would happen if the two sides were to fight to annihilation.

Representing combat with a production function is consistent with
Lanchester-type models of war, which have been the foundation of
combat models since the 1960's
[@Lepingwell1987; @epstein1985calculus; @Anderton1992]
^[The most extensive reference on the Lanchester Law's is @Taylor1983 and @Taylor1983a.
 @Wrigge1995 and @Helmbold1993 are extensive bibliographies of all literature related to Lanchester up to the mid-1990s]
In Lanchester-type models, the casualty rates of combatants are
represented as a pair of differential equations in which the casualty
rates of each belligerent are a function of the size of its own and
its opponent's forces.  This system can be solved to find the
evolution of the force sizes over time.  The Lanchester model is not a
complete model of battles. It does not account for geography, the
termination rules, allocation of forces, etc.  However, the attrition
process is fundamental to war.  The strategic and tactical decisions
to start, continue, or terminate fighting and to retreat or advance of
the belligerents are conditioned on the attrition that occurs when
they do fight.

While there have been many works which estimate Lanchester-type models
with casualty data from one or many battles, they do not account for
the variation in parameters between battles [@Engel1954; @Willard1962; @Weiss1966; @hartley1990; @Helmbold1971a; @Helmbold1961; @Helmbold1964; @Hartley1995; @HartleyHelmbold1995].
@Helmbold1995 is the intellectual predecessor of this
paper, in that it recognizes that different battles could have
different parameters. However, that paper only includes a linear time
trend in its model.  This is a restrictive assumption on how the
attrition technology changes over time. It still assumes that the
parameters relating the casualty rate of a belligerent and the forces
remains constant across all wars.  This paper allows all the
parameters to vary between battles and wars to the extent that the
data will allow such variation.  Rather than trying to ``test'' the
Lanchester model, this paper uses the Lanchester model as a
description of the relationship between force sizes and casualties,
and estimates how this relationship varies between battles.

Model comparisons of the hierarchical model against pooled estimates
imply that the variation between wars and battles is
important. However, the amount of data in insufficient to provide
tight estimates of the parameters for each war.  However, despite
this, a few interesting results appear in the data.

This paper addresses some of the methodological issues in modeling tactical conflicts.
In the empirical literature there are disagreements as to whether to model battles ...

At the minimum, battles consist of two processes: an attrition process by which casualties are sustained and a quitting proces in which at least one of the belligerents chooses to disengage from combat.
The first process is largely a physical one determined by the military technology of the day.
The second process is largely a strategic (in the game theoretic, not military sense) one.

A more minimal model of a battle is a contest.
However, the problem with this is that it is ambiguous as to whether the effort in the contest corresponds to the forces committed to the battle or the casualties endured in the battle (cite Hwang).
That ambiguity arises because the contest game does not account for the attrition process.

Attrition is a dynamic process, and the functional form of the force inputs is particularly important in determining the eventual winner as well as the importance of the other covariates.

1. The strengths of each side in the battle are not just confounding covariates, but the essential variables in the attrition process.
The functional form of the strengths influences the importance of other variables that are included in the productivity term, and not just in the statistical signficance sense.
Give example with 

2. The functional form of this process can determine how decisive

The difficulty, as with all battles, is that the quantity and quality of battle data.
This paper will use the CDB90 data because it still represents the most complete battle data currently in existence.

In battle, military effectiveness can be

1. Allocation: Ability to bring strength to the battle
2. Attrition: Ability to incur casualties 
3. Quitting: Ability to sustain casualties before leaving the battle



# Lanchester Models {#lanchester}

In an attrition process, casualties (changes in force sizes) are a function of the force sizes.
Thus by at its essence attrition is a system a dynamic processes which can be represented as a system of difference or differential equations.
Lanchester-type models represent the attrition process as a system of differential equations,
$$
\begin{aligned}[t]
\frac{d\,y_1}{d\,t} &= -f_1(y_1, y_2) \\
\frac{d\,y_2}{d\,t} &= -f_2(y_1, y_2)
\end{aligned}
$$
In one of the original formulations by @Lanchester1916, the later named Lanchester Square Law, each force's casualty rate is proportional to the size of the opposing force,
$$
\begin{aligned}[t]
\frac{d\,y_1}{d\,t} &= -\alpha_1 y_2 \\
\frac{d\,y_2}{d\,t} &= -\alpha_2 y_1
\end{aligned}
$$
In the alternative formulation by @Lanchester1916, the so-called Lanchester Linear Law, each force's casualty rate is proportional to the product of its own force size and the opposing force size.
$$
\begin{aligned}[t]
\frac{d\,y_1}{d\,t} &= -\alpha_1 y_1 y_2 \\
\frac{d\,y_2}{d\,t} &= -\alpha_2 y_2 y_1
\end{aligned}
$$
These systems of differential equations can be solved for ...
In addition to the Lanchester Linear and Square Laws, there have been many different functional forms proposed for $f_1$, and $f_2$,^[See [@Taylor1983, Chapter 2; @Helmbold1994, Appendix B] for overviews of various attrition functions and their properties.]

|                   | $f_1$                              | $f_2$                              |                                 |
|-------------------+------------------------------------+------------------------------------+---------------------------------|
| Lanchester Square | $\alpha_1 y_2$                     | $\alpha_2 y_1$                     | [@Lanchester1916]               |
| Lanchester Linear | $\alpha_1 y_1 y_2$                 | $\alpha_2 y_1 y_2$                 | [@Lanchester1916; @Osipov1995]  |
| Logarithmic       | $\alpha_1 y_1$                     | $\alpha_2 y_2$                     | [@Peterson1967]                 |
| $p$-linear        | $\alpha_1 (y_1 y_2)^p$             | $\alpha_2 (y_1 y_2)^p$             | [@Helmbold1965]                 |
| Taylor-Helmbold   | $\alpha_1 y_1^{1 - w_1} y_2^{w_2}$ | $\alpha_2 y_2^{1 - w_2} y_1^{w_2}$ | [@Helmbold1965; @Taylor1983]    |
| Franks-Partridge  | $\alpha_1 \min(y_1, y_2)$          | $\alpha_2 \min (y_1, y_2)$         | [@FranksPartridge1993]          |
| Additive          | $\alpha_1 y_1 + \beta_2 y_2$       | $\alpha_2 y_2 + \beta_2 y_2$       | [@MorseKimball1951]             |
| Mixed             | $\alpha_1 y_2$                     | $\alpha_2 y_1 y_2$                 | [@Brackney1959; @Deitchman1962] |

: Common attrition functions in Lanchester-type models. 

Many of the functional forms were derived from micro-foundational models of particular forms of combat.
Explanation of the Lanchester linear law.
Lanchester used the linear law as a representation of ancient warfare.
Lanchester used the square law as a representation of modern warfare, in which individuals in a group can concentrate fire on other individuals.

There are two things to note.
First, although this paper will generally refert to the "technology of war" to be functional form of the relationships between the forces and casualties, these descriptions and micro-level models bridge the gap between physical weapons and their employment and the functional forms used in these models.
Second, although these micro-foundational models of combat are sufficient to imply certain functional forms in the relationship between force sizes and casualties, they are not necessary for these relationships to hold.
Helmbold or Taylor have quotes about this.
Unlike claims of @Homer-Dixon1987, these relationships can be characterized for aggregate inputs because the micro-foundational models that imply a certain functional form are neither unique nor necessary, something that is recognized in operations research [@Taylor1983].

These functional forms of casualty rates are analagous to production functions, and in many cases have similar functional form to common production functions, e.g. the Cobb-Douglas.
In the case of attrition, the inputs are the force sizes and the outputs are the changes in force sizes, casualties.
However, unlike production forces, most of these attrition function forms were derived from simple models of combat rather than conditions which the function must satisfy (see @Helmbold1965 and @AdamsMesterton-Gibbons2003 for exceptions).

Most of the attrition functions listed in the table are special cases of the Cobb-Douglas function,
$$
\frac{d\,y_i}{d\,t} = - \alpha_i \left(y_{-i}^{\delta_i} y_{i}^{1 - \delta_i}\right)^{\nu_i}
$$
where $\delta_i \in [0, 1]$, $\alpha_i \geq 0$, and $\nu \geq 0$.
The parameter $\alpha$ is the attrition rate coefficient.
The parameter $\delta$ influences the influence of friendly and enemy forces on attrition.
This parameterization nests the Lanchester square ($\delta = 1$), Lanchester linear ($\delta = \frac{1}{2}$, $\nu = 2$),
the logarithmic ($\delta = 0$, $\nu = 0$), the $p$-linear ($\delta = \frac{1}{2}$, $\nu = 2 p$), the Taylor-Helmbold ($\nu = 1$), and the mixed function ($\delta_1 = 1$, $\delta_2 = \frac{1}{2}$).
The parameter $\nu$ controls the returns to scale.
When $\nu = 0$ there are constant returns to scale, so the casualty rate increases in proportion to the increase in force sizes, $f(k y_i, k y_i) = k f(y_i, y_i)$.
When $\nu < 0$, there are decreasing returns to scale, so the casualty rate increases by less than the increase in force sizes, $f(k y_i, k y_i) < k f(y_i, y_i)$.

The constant elasticity of scale function further generalizes the Cobb-Douglas [@ArrowCheneryMinhasEtAl1961; @Kmenta1967; @Henningsen2012],
$$
\frac{d\,y_i}{d\,t} = - \alpha_i \left(\delta_i y_{-i}^{\rho_i}  + (1 - \delta_i) y_i^{\rho_i} \right)^{\frac{\nu_i}{\rho_i}}
$$
Where $\alpha_i \geq 0$, $\delta \in [0, 1]$, $\rho \in [-1, 0) \cup (0, \infty)$.
The CES production function includes the Cobb Douglas ($\rho \to 0$), the Leontief production function $\rho \to \infty$, and the linear production function ($\rho \to -1$).
Thus the CES production function includes all the remaining standard production funtions.
The CES can be approximated by the Kmenta approximation,
$$
\ln d\,y = \ln \alpha + \nu \delta \ln y_{-i} + \nu (1 - \delta) \ln y_i - \frac{\rho \nu}{2} \delta (1 - \delta)(\ln y_{-i} - \ln y_i)^2
$$



## Estimation

The simplest approach to estimating the system of differential equations is to take the finite difference approximation of the derivative,
<!-- ^[Alternatives are to analytically solve for $y_1(t)$  and $y_2(t)$, which is possible for many of the equations in table XXX, or to numerical integration approaches such as Runge-Kutta.] -->
<!-- ^[An issue with this finite difference approximation is that the error in the approximation increases with duration of the battle.] -->
$$
\begin{aligned}[t]
\frac{\Delta y_1}{T} &= f_1(y_1, y_2) \\
\frac{\Delta y_2}{T} &= f_1(y_1, y_2)
\end{aligned}
$$

In many cases, political scientists are not directly interested in the relationship between forces and casualties, but instead how other factors, such as regime type or economic development influence military effectiveness (cites).
However, in general, ignoring functional form of omitted variable bias [@SignorinoYilmaz2003, also cite Signorino Kenkel].
When modeling the attrition process, this issue is particularly important because the dynamic nature of the model means thatthe form of the attrition function always influences the effect of factors on casualties, even when these other factors and the forces are separable in the attrition rate functions.

To understand the importance of the form of the attrition form on the relationship between attrition coefficients and the initial forces, consider two special cases which are especially analytically tractable: the Lanchester linear and Lanchester square laws.
The attrition equations in the Lanchester square law are,
$$
\begin{aligned}[t]
\frac{d\,y_1}{d\,t} &= \alpha_1 y_2 \\
\frac{d\,y_1}{d\,t} &= \alpha_2 y_1 \\
\end{aligned}
$$
These differential equations can be solved for who will win in a fight to the finish.
The two sides are equally matched (reaching zero force sizes at an equal time) when [@Taylor1983, p. 78],
$$
\label{eq:3}
\frac{y_1}{y_2} = \sqrt{\frac{\alpha_1}{\alpha_2}}
$$
In \ref{eq:3}, if the force ratio is multiplied by $a$, then then relative attrition effectiveness ($\frac{\alpha_1}{\alpha_2}$) need to be multiplied by $a^2$ to main equality.
The importance of the opponent's force, $y_2$, and the attrition coefficient ratio, $\frac{\alpha_1}{\alpha_2}$, can be judged by the derivatives of $y_1$ with respect to each.

The attrition equations in the Lanchester linear law,
$$
\begin{aligned}[t]
\frac{d\,y_1}{d\,t} &= \alpha_1 y_1 y_2 \\
\frac{d\,y_1}{d\,t} &= \alpha_2 y_1 y_2 \\
\end{aligned}
$$
In this, the two sides are equally matched when [@Taylor1983, p. 98],
$$
\label{eq:4}
\frac{y_1}{y_2} = \frac{\alpha_1}{\alpha_2}
$$
Unlike the square law, if the force ratio is multiplied by $a$, then the relative attrition effectiveness need only be multiplied by $a$ to maintain equality in \ref{eq:3}.
Thus, in the linear law changes in the attrition coeficients are equivalent to changes in force sizes, while in the square law changes in the force size ratio must be compensated by squared changes in the relative attrition rate coefficient.



# Loss-Exchange Ratios {#ler}

Following @BiddleLong2004, most political science papers do not estimate the differential equations, but instead estimate the log loss-exchange ratio, $\ln \left( \frac{x_1}{x_2} \right)$.
The general regression form, ignoring all non-force variables,
$$
\label{eq:1}
\ln (\Delta y_1) - \ln(\Delta y_2) = \alpha + \beta \left( \frac{y_1}{y_1 + y_2} \right)
$$
In this, the log difference of casualties is a linear function of the *ratio* of the forces.
The $\alpha$ term includes non-force factors which influence the loss exchange ratio, such as democracy, culture, etc.

Unusually, the attrition rate equations that are implied by this loss-exchange ratio form is not implied by any of the usual Lanchester laws listed in table ....
$$
\label{eq:2}
\begin{aligned}[t]
\frac{d\, y_1}{d\,t} &= \alpha_1 f(y_1, y_2) \exp \left( \beta_1 \frac{y_1}{y_1 + y_2} \right) \\
\frac{d\, y_2}{d\,t} &= \alpha_2 f(y_1, y_2) \exp \left( \beta_2 \frac{y_1}{y_1 + y_2} \right)
\end{aligned}
$$
The parameters in \ref{eq:1} follow from \ref{eq:2}, $\alpha = \ln \alpha_1 + \delta_1 - \ln \alpha_2 - \delta_2$, and $\beta = \beta_1 - \beta_2$.
The differential equations implied \ref{eq:1} are not unique.

Analysis of the the loss exchange ratio rather than raw casualties has some favorable properties.
First, by dividing casualty rates it marginalizes out battle specific effects, so the correlation between $\Delta y_1$ and $\Delta y_2$ can be ignored.
Second, the disivion of casualties cancels out the battle duration, so it does not need to be mechanically controlled for.
However, controlling for the duration of the battle may proxy for some other unobserved differences between battles.
^[A more principled estimation approach would estimate battle duration as part of competing hazards model, along with the casualty rates]

Unfortunately, loss-exchange ratios do not necessarily control for the total size of the battle.
In fact, for the Cobb-Douglas form of attrition the use of loss-exchange ratios does not removes the dependence on the scale of the battle, while also making the returns to scale parameter unidentified.
Smaller units, such as companies and battaltions consist almost entirely of combat troops, which are almost all exposed to enemy fire, and thus potential casualties.
Larger units, such as corps or armies, include a larger number of non-combat personnel as well as component units held in reserve; thus there are less personnel exposed to fire and fewer potential casualties.
However, the use of the loss-exchange ratio does not completely control for this effect, depending on the functional form of the original attrition equations.
The loss exchage ratio implied by the Cobb-Douglas form attrition equations is 
$$
\ln \Delta y_1 - \ln \Delta y_2 = (\ln \alpha_1 - \ln \alpha_2) + \nu (1 - 2 \delta) y_1 + \nu (2 \delta  - 1) y_2
$$
The estimate of the log loss exchange ratio is still dependent on $\nu$, but now the values of $\nu$ and $\delta$ are unindentified.
^[This is true for $\delta \in (0, 1)$.
Note that $1 - 2 \delta = -(2 \delta - 1)$.
Let $\gamma = 1 - 2 \delta$.
For $\nu'$, $\delta'$, there exists a $\delta^* = c \delta'$ and $\nu^* = \frac{\nu'}{c}$ such that $\nu' \delta' = \nu^* \delta^*$.
]
However, this unidentification does not necessarily occur with other functional forms of the attrition function.
Nevertheless, it is troubling that it should occur in the most commonly used theoretical form of attrition functions.
This shows that on its own, the loss-exchange ratio does not marginalize out the battle scale.
And in fact, theoretically we should assume that battle scale will have an impact on the loss-exchange ratio.
When there are decreasing returns to scale in casualties, then there are

Another issue with this estimation is that loss exchange ratios are unable to directly recover the casualties per period.
This could presumably be estimated by plugging in the observed $\Delta y_1 + \Delta y_2$.

![Plot of log loss exchange ratio $\ln \left( \frac{\Delta y_{a}}{\Delta y_{d}} \right)$ to log total strength $\ln (y_a y_d)$](figure/ller_geom_str.pdf) 



# Data {#data}

The U.S. Army's Concept Analysis Agency's CDB90 dataset is the source
of data on battles in this paper @cdb90.
^[The CDB90 dataset is often referred to as the HERO
  dataset. It is in fact, a successor to the 1984 HERO dataset.  CDB90
  incorporates revisions to the HERO dataset based on independent
  reviews and incorporates an additional 60 battles from
  Anderson @cdb90 and @Helmbold1995.]
The CDB90 dataset contains 660 battles from 1600-1984 (from the Dutch Revolt to the 1982 Lebanon War).
The battles are from U.S., European European and Israeli Wars, the full list of wars appears and number of battles from each appears in Table ???
This work uses a subset of 624 battles after dropping duplicate battles and belligerents with missing
data.

The CDB90 database has been criticized for its quality and selection of wars and battles.
However, it is the best data source on data on battles in interstate wars.
^[See @Biddle2001, @BiddleLong2004, @Brooks2003, @Mearsheimer1989, and @Ramsay2008 for an overview of this debate.]
@BiddleLong2004 summarizes this opinion


> 	The resulting data are not perfect --- coding errors doubtless
> 	remain, HERO’s selection rationale for the battles included is
> 	opaque, and the data set provides little information on the
> 	technical sophistication of the weapons it treats (weapons data are
> 	limited to the numbers of systems of each major type but not their
> 	particular makes, models, or performance). On the other hand, CDB90
> 	offers the only meaningful data available on the outcomes of
> 	battles, as distinct from wars; it has already played a role in the
> 	democratic effectiveness literature; and the great majority of the
> 	spot-checked values were in reasonable consistency with the official
> 	historical record

CDB90 has been criticized for the quality of its data [@Desch2002; @Mearsheimer1989].
However, the major source of criticism in the reports that these sources cite was in subjective codings of concepts such as leadership and morale, rather than the casualty and strength values that are used in this work @epstein1989.
Second, while independent reviews disagreed with the HERO data, they independent reviews also disagreed with each other, suggesting that the problem is one of measurement error rather than systematic bias [@epstein1989].
Third, the independent reviews used in the critiques were of the HERO data, not the CDB90 data. CDB90 is a
revision of the 1984 HERO dataset, and includes corrections to some of the problems that were identified reviews [@cdb90, README.txt].
Fourth, although CDB90 data may have high amounts of measurement error on casualties and strengths, it follows the gold-standard of datasets by reporting observation-level high and low estimates for the casualties and strength data.
Finally, measurement error in these quantities appears to be endemic to war, and a problem not just for the researcher, but also the participants [@Helmbold1993, section 2.5].

The second criticism of CDB90 is regarding the selection of its sample of wars and battles.
The sample of wars is wars is exclusively American, European, and Israeli, although it includes a few colonial wars [@BiddleLong2004, p. 533; @Biddle2004].
I am primarily interested about the variation between and within wars.
That these data come from a more homogenous sample of wars and battles than the overall population of wars, the results here will understate variation in the technology of combat.
Alternatively, this sample of battles can be considered a better sample than choosing all wars because CDB90 has already pre-selected a set of wars with similar characteristics and participants, and thus the variation over time is more likely due
to technological innovation rather than wars fought in different places at different times.
The choice of this sample means that the extrapolation beyond the set of battles considered is not advisable.

A related but distinct complaint of that the CDB90 database is regarding variation in the level of detail in which wars are covered.
The hierarchical method used in this work mitigates this issue by grouping observations at the war level.
Wars with more battles will have more precise estimates, but by grouping wars, it mitigates the influence of wars with large numbers of battles on the overall estimate relative to a pooled estimate.



# Statistical {#statisticalmodel}

As described in Section XXX, the differential equations can be easily estimated by the logarithm in the linearization of of the differential equations,
$$
\label{eq:5}
\begin{aligned}[t]
\ln \Delta y_{i,j} - \ln D_i &=
\alpha_{i,j}
+ \nu_{i,j} \pi_{i,j} \ln y_{i,-j}
+ \nu_{i,j} (1 - \pi_{i,j}) \ln y_{i,j}
+ \epsilon_{i,j}
\end{aligned}
$$
where $i$ indexes battles, $j \in \{a, d\}$ indexes belligerents (attacker, defender),
$\Delta y$ is casualties, $y_{i,j}$ is the size of the belligerents' own forces, and $y_{i,-j}$ is the size of opposing forces, and $D_i$ is the duration of the battle.
The parameter $\alpha$ is a battle specific attrition coefficient.
The parameter $\nu \in [0, \infty)$ is the battle returns to scale.
The parameter $\pi \in [0, 1]$ is the force share parameter which determines the sensitivity of casualties to friendly and enemy forces.

Since features in a battle will influence the casualties of both belligerents, the error terms $\epsilon_{i, j}$ are allowed to be correlated,
$$
\begin{pmatrix}
\epsilon_{i,a} \\
\epsilon_{i,d}
\end{pmatrix}
\sim
N \left(
0,
\begin{pmatrix}
\sigma_{\epsilon}^2 & \rho_{\epsilon} \sigma_{\epsilon} \\
\rho_{\epsilon} \sigma_{\epsilon} & \sigma_{\epsilon}^2 
\end{pmatrix}
\right)
$$

Equation \ref{eq:5} is written in the most general form, in which each parameter takes different values for each battle and belligerent.
However, obviously, Equation \ref{eq:5} is not enough data to estimate individual parameters for each battle and belligerent.
The typical approach would be to assume constant parameters or to model some of the parameters as functions of covariates.
I will impose some on the parameters in order to estimate them, while still allowing for flexibility in the parameters over time.
The general approach to imposing structure on these parameters is that parameters of battles in the same war and close in time are more similar to each other.

Alternatively, a model with parameters per war, 
$$
\begin{aligned}
\alpha_{ij} &= \gamma_{\mathtt{war}[i]} + \delta_{\mathtt{war}[i]} I(j = a) + \beta_{\mathtt{belligerent}[i,j]} - \beta_{\mathtt{belligerent}[i,-j]} \\
\gamma_{w} & \sim \mathcal{N} \left( \rho_{\gamma} \gamma_{w-1}, t(w, w - 1)^2 \sigma_{\gamma}^2 \right) \\
\delta_{w} & \sim \mathcal{N} \left( \rho_{\delta} \delta_{w-1}, t(w, w - 1)^2 \sigma_{\delta}^2 \right) \\
\beta_{b} &\sim \mathcal{N} \left(0, \sigma^2_\beta \right) \\
\nu_{ij} & = \exp (\tilde \nu_{\mathtt{war}[i]}) \\
\tilde\nu_{w} & \sim \mathcal{N} \left( \rho_{\nu} \tilde \nu_{w - 1}, t(w, w - 1)^2  \sigma_{\nu}^2 \right) \\
\pi_{ij} & =  \frac{1}{1 + \exp(\tilde \pi_{\mathtt{war}[i]})} \\
\tilde\pi_{w} & \sim \mathcal{N} \left( \rho_{\pi} \tilde \pi_{w - 1}, t(w, w - 1)^2 \sigma_{\pi}^2 \right)
\end{aligned}
$$

# Appendices {#appendices}


## Wars {#wars}


# References

<!-- Local Variables: -->
<!-- pandoc-mode: t -->
<!-- orgtbl-mode: t -->
<!-- End: -->

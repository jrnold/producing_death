#!/usr/bin/env Rscript
args <- commandArgs(TRUE)
outfile <- file(args[1], 'w')

RDATA_DIR <- 'rdata'
R_DIR <- 'R'
R_PATTERN <- '[^.].*\\.R$'

CDB13_DEP <- "data/cdb13/datapackage.json"
rdata_key_path <- function(key) {
    file.path(RDATA_DIR, key)
}

cat("robjects = \n\n", file = outfile)
filelist <- dir(R_DIR, pattern = R_PATTERN, full.names = TRUE)
for (file in filelist) {
    cat(file, "\n")
    envir <- suppressPackageStartupMessages({source_env(file)})

    dependencies <- file
    if (! is.null(envir$CDB13)) {
        if (envir$CDB13) {
            dependencies <- c(dependencies, CDB13_DEP)
        }
    }
    if (! is.null(envir$MODEL)) {
        dependencies <- c(dependencies, STAN_MODEL(envir$MODEL))
    }
    if (! is.null(envir$INIT)) {
        dependencies <- c(dependencies,
                          file.path('data/init', sprintf('%s.json', envir$INIT)))
    }
    dependencies <- c(dependencies, lapply(envir$DEP_KEYS, rdata_key_path))

    key <- basename(tools::file_path_sans_ext(file))
    keypath <- sprintf('%s/%s', RDATA_DIR, key)
    cat(sprintf("robjects += %s\n\n", keypath), file = outfile)
    cat(sprintf("%s : %s\n\n", keypath, paste(dependencies, collapse = " ")),
        file = outfile)

}
close(outfile)

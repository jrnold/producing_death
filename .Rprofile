suppressPackageStartupMessages({
    library("filehash")
    library("jsonlite")
})

DIR_INITVALS <- 'data/init'

CDB13 <- function(key) {
  path <- "data/cdb13/data"
  read.csv(file.path(path, sprintf("%s.csv", key)))
}

STAN_MODEL <- function(model) {
    sprintf('stan/%s.stan', model)
}

RDATA <- dbInit("rdata", "RDS")

source_env <- function(file, chdir = FALSE,
                       keep.source = getOption("keep.source.pkgs"), ...) {
    envir <- new.env(...)
    sys.source(file, envir=envir, chdir = chdir, keep.source = keep.source)
    envir
}

stan_resample <- function(model, n = 4, replace=FALSE) {
    n_kept <- model@sim$n_save - model@sim$warmup2
    iters <- sample.int(n_kept, n, replace=replace)
    pars <- grep("__$", model@model_pars, value=TRUE, invert=TRUE)
    plyr::llply(iters,
          function(i) {
              x <- llply(pars,
                         function(parname) {
                             x <- extract(model, parname)[[1]]
                             d <- dim(x)
                             ndim <- length(d)
                             dlist <- list(i)
                             if (ndim > 1) {
                                 for (j in 2:ndim) {
                                     dlist[[j]] <- seq_len(d[j])
                                 }
                             }
                             do.call(`[`, c(list(x), dlist))
                         })
              names(x) <- pars
              x
          })
}

stanfit_resample <- function(model, n = 4, replace=FALSE) {
    n_kept <- model@sim$n_save - model@sim$warmup2
    iters <- sample.int(n_kept, n, replace=replace)
    pars <- grep("__$", model@model_pars, value=TRUE, invert=TRUE)
    plyr::llply(iters,
          function(i) {
              x <- llply(pars,
                         function(parname) {
                             x <- extract(model, parname)[[1]]
                             d <- dim(x)
                             ndim <- length(d)
                             dlist <- list(i)
                             if (ndim > 1) {
                                 for (j in 2:ndim) {
                                     dlist[[j]] <- seq_len(d[j])
                                 }
                             }
                             do.call(`[`, c(list(x), dlist))
                         })
              names(x) <- pars
              x
          })
}

#' Generate new startvals from stanfit object
#'
#' Given the results of a MCMC run stored in a \code{stanfit} object,
#' generate a values to use as initial values for another run.
#'
#' @param x \code{stanfit} object
#' @param pars \code{character} Paratmer names
#' @param .FUN \code{function} to generate starting values from
#' the parameter samples.
#' @return \code{list} of \code{array} objects for each parameter.
#' @export
stanfit_get_new_startvals <- function(x, pars = x@model_pars, .FUN=mean) {
    ret <- list()
    for (i in pars) {
        parvals <- extract(x, i)[[1]]
        if (length(dim(parvals)) > 1) {
            ret[[i]] <- apply(parvals, seq_along(dim(parvals))[-1], .FUN)
        } else {
            ret[[i]] <- .FUN(parvals)
        }
    }
    ret
}

write_init <- function(x, key) {
    cat(toJSON(x), file = file.path(DIR_INITVALS, sprintf('%s.json', key)))
}

read_init <- function(key) {
    initfile <- file.path(DIR_INITVALS, sprintf('%s.json', key))
    if (file.exists(initfile)) {
        fromJSON(initfile, simplifyDataFrame = FALSE)
    } else {
        NULL
    }
}
                      
